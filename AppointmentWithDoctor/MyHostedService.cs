﻿using System.Threading;
using System.Threading.Tasks;
using AppointmentWithDoctor.Services;
using Microsoft.Extensions.Hosting;

namespace AppointmentWithDoctor
{
    public class MyHostedService : IHostedService
    {
        private ICommunicationUserService _communicationUserService;
        public MyHostedService(ICommunicationUserService communicationUserService)
        {
            _communicationUserService = communicationUserService;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _communicationUserService.Run();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}