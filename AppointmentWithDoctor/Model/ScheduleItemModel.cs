﻿using System;

namespace AppointmentWithDoctor.Model
{
    public class ScheduleItemModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}