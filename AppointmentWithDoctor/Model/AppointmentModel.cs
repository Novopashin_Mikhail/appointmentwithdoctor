﻿using System;
using AppointmentWithDoctor.Repository.Entities;

namespace AppointmentWithDoctor.Model
{
    public class AppointmentModel
    {
        public int Id { get; set; }
        public PatientModel Patient { get; set; }
        public DoctorModel Doctor { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public static AppointmentModel GetFromEntity(AppointmentEntity entity)
        {
            var model = new AppointmentModel
            {
                Start = entity.Start,
                End = entity.End
            };
            return model;
        }
    }
}