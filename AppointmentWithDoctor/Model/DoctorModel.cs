﻿using System.Collections.Generic;
using System.Linq;
using AppointmentWithDoctor.Repository.Entities;

namespace AppointmentWithDoctor.Model
{
    public class DoctorModel
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public string Fio { get; set; }
        public SpecialityModel Speciality { get; set; }
        public List<AppointmentModel> Appointments { get; set; }
        public List<ScheduleItemModel> Schedule { get; set; }
        
        public static DoctorModel GetFromEntity(DoctorEntity entity)
        {
            var doctor = new DoctorModel();
            doctor.Id = entity.Id;
            doctor.Fio = entity.Fio;
            doctor.Speciality = new SpecialityModel
            {
                Title = entity.Speciality.Title
            };
            doctor.Appointments = entity.AppointmentEntities
                .Select(x => new AppointmentModel
                {
                    Patient = new PatientModel
                    {
                        Fio = x.Patient.Fio
                    },
                    Start = x.Start,
                    End = x.End
                }).ToList();
            doctor.Schedule = entity.DoctorScheduleEntities.Schedule.ScheduleItems
                .Select(x => new ScheduleItemModel
                {
                    Start = x.Start,
                    End = x.End
                }).ToList();
            return doctor;
        }
    }
}