﻿using AppointmentWithDoctor.Repository.Entities;

namespace AppointmentWithDoctor.Model
{
    public class SpecialityModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static SpecialityModel GetFromEntity(SpecialityEntity entity)
        {
            var res = new SpecialityModel
            {
                Id = entity.Id,
                Title = entity.Title
            };
            return res;
        }
    }
}