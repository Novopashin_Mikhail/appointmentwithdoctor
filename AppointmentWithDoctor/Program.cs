﻿using AppointmentWithDoctor.Repository;
using AppointmentWithDoctor.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AppointmentWithDoctor
{
    class Program
    {
        static void Main(string[] args)=> CreateHostBuilder(args).Build().Run();
        
        private static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureServices((context, services) =>
                {
                    services.AddTransient<ICommunicationUserService, CommunicationUserService>();
                    services.AddTransient<IAppointmentService, AppointmentService>();   
                    services.AddTransient<IRepository, Repository.Repository>();
                    services.AddHostedService<MyHostedService>();
                    services.AddDbContext<Context>((services, options) =>
                    { });
                });
    }
}