﻿using System;
using System.IO;
using AppointmentWithDoctor.Repository.Entities;
using Microsoft.EntityFrameworkCore;

namespace AppointmentWithDoctor.Repository
{
    public class Context: DbContext
    {
        private string path = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.Parent.FullName, @"AppointmentWithDoctor/data.db");
        public Context(DbContextOptions<Context> options): base(options){}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($@"Data Source={path}");
        }

        public DbSet<AppointmentEntity> Appointment { get; set; }
        public DbSet<DoctorEntity> Doctor { get; set; }
        public DbSet<PatientEntity> Patient { get; set; }
        public DbSet<SpecialityEntity> Speciality { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<DoctorEntity>()
                .HasOne(s=> s.Speciality)
                .WithMany(d => d.Doctors);
            
            builder.Entity<AppointmentEntity>()
                .HasKey(a => new { a.DoctorId, a.PatientId });  
            builder.Entity<AppointmentEntity>()
                .HasOne(x => x.Doctor)
                .WithMany(y => y.AppointmentEntities)
                .HasForeignKey(z => z.DoctorId);  
            builder.Entity<AppointmentEntity>()
                .HasOne(x => x.Patient)
                .WithMany(y => y.AppointmentEntities)
                .HasForeignKey(z => z.PatientId);
            
            builder.Entity<DoctorScheduleEntity>()
                .HasKey(x => new { x.DoctorId, x.ScheduleId });
            builder.Entity<DoctorScheduleEntity>()
                .HasOne(x => x.Doctor)
                .WithOne(y => y.DoctorScheduleEntities)
                .HasForeignKey<DoctorScheduleEntity>(z => z.DoctorId);
            builder.Entity<DoctorScheduleEntity>()
                .HasOne(x => x.Schedule)
                .WithMany(y => y.DoctorScheduleEntities)
                .HasForeignKey(z => z.ScheduleId);
          
            builder.Entity<ScheduleEntity>()
                .HasMany(x => x.ScheduleItems)
                .WithOne(y => y.Schedule);
        }
    }
}