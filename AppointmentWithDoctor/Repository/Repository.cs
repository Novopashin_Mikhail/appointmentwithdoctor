﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppointmentWithDoctor.Model;
using AppointmentWithDoctor.Repository.Entities;
using Microsoft.EntityFrameworkCore;

namespace AppointmentWithDoctor.Repository
{
    public class Repository : IRepository
    {
        private Context _context;
        public Repository(Context context)
        {
            _context = context;
        }

        public async Task<IList<DoctorModel>> GetDoctorsAsync()
        {
            var doctorEntity = await _context.Doctor
                .Include(x => x.Speciality)
                .Include(x => x.AppointmentEntities)
                    .ThenInclude(y => y.Patient)
                .Include(x => x.DoctorScheduleEntities)
                    .ThenInclude(y => y.Schedule)
                        .ThenInclude(z => z.ScheduleItems)
                .ToListAsync();

            return doctorEntity.Select(DoctorModel.GetFromEntity).ToList();
        }

        public async Task<bool> TryAddAppointment(AppointmentModel model)
        {
            try
            {
                var entity = new AppointmentEntity
                {
                    DoctorId = model.Doctor.Id,
                    PatientId = model.Patient.Id,
                    Start = model.Start,
                    End = model.End
                };
                await _context.Appointment.AddAsync(entity);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        public async Task<IList<AppointmentModel>> GetAppointmentsByDoctorAsync(int doctorId)
        {
            return await _context.Appointment.Where(x => x.DoctorId == doctorId)
                .Select(x => AppointmentModel.GetFromEntity(x)).ToListAsync();
        }

        public async Task<DoctorModel> GetDoctorAsync(int doctorId)
        {
            var doctorEntity = await _context.Doctor
                .Include(x => x.Speciality)
                .Include(x => x.AppointmentEntities)
                .Include(x => x.DoctorScheduleEntities)
                .ThenInclude(y => y.Schedule)
                .ThenInclude(z => z.ScheduleItems)
                .FirstOrDefaultAsync();

            return DoctorModel.GetFromEntity(doctorEntity);
        }

        public Task RemoveAppointment(int appointmentId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAppointment(int appointmentId, AppointmentModel newModel)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryAddNewPatient(PatientModel model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdatePatient(int patientId, PatientModel newModel)
        {
            throw new NotImplementedException();
        }

        public async Task<List<SpecialityModel>> GetSpecialityAsync()
        {
            var specialityEntity = await _context.Speciality.ToListAsync();
            return specialityEntity.Select(SpecialityModel.GetFromEntity).ToList();
        }
    }
}