﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace AppointmentWithDoctor.Repository
{
    public class ContextFactory : IDesignTimeDbContextFactory<Context>
    {
        private string path = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.Parent.FullName, @"AppointmentWithDoctor/data.db");
        
        public Context CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();
            optionsBuilder.UseSqlite($@"Data Source={path}");

            return new Context(optionsBuilder.Options);
        }
    }
}