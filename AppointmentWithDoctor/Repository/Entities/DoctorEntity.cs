﻿using System;
using System.Collections.Generic;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class DoctorEntity
    {
        public int Id { get; set; }
        public int SpecialityId { get; set; }
        public string Fio { get; set; }
        public SpecialityEntity Speciality { get; set; }
        public List<AppointmentEntity> AppointmentEntities { get; set; }
        public DoctorScheduleEntity DoctorScheduleEntities { get; set; }
    }
}