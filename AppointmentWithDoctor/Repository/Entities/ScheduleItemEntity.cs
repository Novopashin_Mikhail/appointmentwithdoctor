﻿using System;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class ScheduleItemEntity
    {
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public ScheduleEntity Schedule { get; set; }
    }
}