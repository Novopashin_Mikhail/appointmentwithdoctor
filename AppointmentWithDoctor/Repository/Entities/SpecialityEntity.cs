﻿using System.Collections.Generic;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class SpecialityEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        
        public List<DoctorEntity> Doctors { get; set; }
    }
}