﻿using System;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class DoctorScheduleEntity
    {
        public int Id { get; set; }
        public int DoctorId { get; set; }
        public int ScheduleId { get; set; }
        
        public DoctorEntity Doctor { get; set; }
        public ScheduleEntity Schedule { get; set; }
    }
}