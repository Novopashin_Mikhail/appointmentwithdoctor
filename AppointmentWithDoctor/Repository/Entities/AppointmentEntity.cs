﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class AppointmentEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int DoctorId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DoctorEntity Doctor { get; set; }
        public PatientEntity Patient { get; set; }
    }
}