﻿using System;
using System.Collections.Generic;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class ScheduleEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        
        public List<DoctorScheduleEntity> DoctorScheduleEntities { get; set; }
        public List<ScheduleItemEntity> ScheduleItems { get; set; }
    }
}