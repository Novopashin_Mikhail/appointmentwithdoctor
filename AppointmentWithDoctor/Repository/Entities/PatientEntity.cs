﻿using System.Collections.Generic;

namespace AppointmentWithDoctor.Repository.Entities
{
    public class PatientEntity
    {
        public int Id { get; set; }
        public string Fio { get; set; }
        
        public List<AppointmentEntity> AppointmentEntities { get; set; }
    }
}