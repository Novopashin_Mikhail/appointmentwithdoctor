﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppointmentWithDoctor.Model;

namespace AppointmentWithDoctor.Repository
{
    public interface IRepository
    {
        Task<IList<DoctorModel>> GetDoctorsAsync();
        Task<DoctorModel> GetDoctorAsync(int doctorId);
        Task<IList<AppointmentModel>> GetAppointmentsByDoctorAsync(int doctorId);
        Task<bool> TryAddAppointment(AppointmentModel model);
        Task RemoveAppointment(int appointmentId);
        Task<bool> UpdateAppointment(int appointmentId, AppointmentModel newModel);
        Task<bool> TryAddNewPatient(PatientModel model);
        Task<bool> UpdatePatient(int patientId, PatientModel newModel);
        Task<List<SpecialityModel>> GetSpecialityAsync();
    }
}