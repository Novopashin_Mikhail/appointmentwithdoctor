﻿using System.Threading.Tasks;
using AppointmentWithDoctor.Model;

namespace AppointmentWithDoctor.Services
{
    public interface IAppointmentService
    {
        Task<string> GetDoctorsBySpeciality(string speciality);
        Task<string> AddAppointment(AppointmentModel appointmentModel);
        Task<string> GetSpeciality();
    }
}