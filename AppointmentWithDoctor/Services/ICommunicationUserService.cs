﻿namespace AppointmentWithDoctor.Services
{
    public interface ICommunicationUserService
    {
        void Run();
    }
}