﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppointmentWithDoctor.Model;
using AppointmentWithDoctor.Repository;

namespace AppointmentWithDoctor.Services
{
    public class AppointmentService : IAppointmentService
    {
        private IRepository _repository;
        
        public AppointmentService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<string> GetDoctorsBySpeciality(string speciality)
        {
            bool FindDoctorBySpecialityOrGetAll(DoctorModel x) => x.Speciality.Title.ToLower().Contains(speciality.ToLower());
            var doctors = await _repository.GetDoctorsAsync();
            if (!string.IsNullOrEmpty(speciality))
                doctors = doctors.Where(FindDoctorBySpecialityOrGetAll).ToList();
            doctors = doctors.ToList();
            return await GetMessageForAdministrator(doctors);
        }

        private async Task<string> GetMessageForAdministrator(IEnumerable<DoctorModel> doctors)
        {
            var sb = new StringBuilder();
            foreach (var doctor in doctors)
            {
                var schedule = doctor.Schedule;
                var appointments = doctor.Appointments;
                foreach (var scheduleItem in schedule)
                {
                    sb.AppendLine(
                        $"Врач {doctor.Fio} специальность {doctor.Speciality.Title} работает с {scheduleItem.Start} до {scheduleItem.End}");
                    var thisDayAppointments = appointments.Where(x => x.Start.Date.Equals(scheduleItem.Start.Date));
                    foreach (var thisDayAppointment in thisDayAppointments)
                    {
                        sb.AppendLine(
                            $"        Запись пациента {thisDayAppointment.Patient.Fio} с {thisDayAppointment.Start} до {thisDayAppointment.End}");
                    }
                }
            }

            return sb.ToString();
        }

        public async Task<string> AddAppointment(AppointmentModel appointmentModel)
        {
            var doctorId = appointmentModel.Doctor.Id;
            var doctorModel = await _repository.GetDoctorAsync(doctorId);
            if (!doctorModel.Schedule.Any(x => x.Start.Date.Equals(appointmentModel.Start.Date)))
                return "Врач в этот день не работает";
            if (IsConflictAppointment(appointmentModel, doctorModel.Appointments))
                return "Не верно указан период посещения врача. Конфликт с другими записями.";
            if (await _repository.TryAddAppointment(appointmentModel))
                return "Выполнено";
            return "Не выполнено";
        }

        public async Task<string> GetSpeciality()
        {
            return string.Join(",",(await _repository.GetSpecialityAsync()).Select(x => x.Title));
        }

        private bool IsConflictAppointment(AppointmentModel appointmentModel, List<AppointmentModel> hasAppointments)
        {
            if (!hasAppointments.Any(x => x.Start.Date.Equals(appointmentModel.Start.Date)))
            {
                return false;
            }
            
            foreach (var itemHasAppointment in hasAppointments.Where(x => x.Start.Date.Equals(appointmentModel.Start.Date)))
            {
                var startTime = appointmentModel.Start.TimeOfDay;
                var endTime = appointmentModel.End.TimeOfDay;
                var startHasTime = itemHasAppointment.Start.TimeOfDay;
                var endHasTime = itemHasAppointment.End.TimeOfDay;
                if (startTime < startHasTime && endTime > endHasTime ||
                    startTime > startHasTime && startTime < endHasTime ||
                    endTime > startHasTime && endTime < endHasTime)
                    return true;
            }

            return false;
        }
    }
}