﻿using System;
using System.Linq;
using System.Text;
using AppointmentWithDoctor.Model;

namespace AppointmentWithDoctor.Services
{
    public class CommunicationUserService : ICommunicationUserService
    {
        private IAppointmentService _appointmentService;

        public CommunicationUserService(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        public void Run()
        {
            while (true)
            {
                var inputResult = InputAndRunCommand();
                Console.WriteLine(inputResult);
                if (inputResult.Equals("-exit")) break;
            }
        }
        
        string InputAndRunCommand()
        {
            Console.WriteLine("Введите команду, для помощи введите -help");
            var input = Console.ReadLine();
            if (input.Contains("-show"))
            {
                var speciality = input.Split(' ').Last();
                return _appointmentService.GetDoctorsBySpeciality(speciality).GetAwaiter().GetResult();
            } else if (input.Contains("-addAppointment"))
            {
                var startString = input.Split(' ').Last();
                var startDateTime = DateTime.ParseExact("2020-04-29 13:00:00", "yyyy-MM-dd HH:mm:ss",
                    System.Globalization.CultureInfo.InvariantCulture);
                var endDateTime = startDateTime.AddMinutes(30);
                var appoitmentModel = new AppointmentModel
                {
                    Patient = new PatientModel
                    {
                        Id = 1
                    },
                    Doctor = new DoctorModel
                    {
                        Id = 2
                    },
                    Start = startDateTime,
                    End = endDateTime
                };
                return _appointmentService.AddAppointment(appoitmentModel).GetAwaiter().GetResult();
            } else if (input.Contains("-exit"))
            {
                return "exit";
            } else if (input.Contains("-help"))
            {
                var sb = new StringBuilder();
                sb.AppendLine("-show <название специальности> для показа докоторов");
                sb.AppendLine(
                    "-addAppointment <время приема> добавит запись для пациента с id=1 и доктора id=2 на указанное время");
                sb.AppendLine("-exit выход из приложения");
                sb.AppendLine(
                    $"Доступные специальности {_appointmentService.GetSpeciality().GetAwaiter().GetResult()}");
                return sb.ToString();
            }

            return input;
        }

    }
}